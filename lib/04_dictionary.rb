class Dictionary
  attr_accessor :entries, :keywords

  def initialize
    @entries = {}
  end

  def add(definition)
    if definition.class == String
      definition = {definition => nil}
    end

    definition.each {|key, value| @entries[key] = value}
  end


  def keywords
    keys = []
    @entries.each {|key, value| keys << key}
    keys.sort
  end

  def include?(key)
    self.keywords.include?(key)
  end

  def find(key_prefix)
    found_entries = @entries.select {|key, value| key[0...key_prefix.length] == key_prefix}
  end

  def printable
    a=@entries.sort.map {|key, value| "[#{key}] \"#{value}\""}
    a.join("\n")
  end
end
