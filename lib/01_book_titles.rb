class Book
	IGNORED_WORDS = ["and", "or", "the", "over", "in", "of", "a", "an"]
	attr_reader :title
	def title=(words)
		@title = Book.titleize(words)
	end
	private

	def self.titleize(title)
		cap_array = []
		title.split.each_with_index do |word, i|
			cap_array << cap_word(word, (i==0))
		end
		cap_array.join(" ")
	end

	def self.cap_word(title, first_word)
		return title if IGNORED_WORDS.include?(title) && !first_word
		letter_array = title.split("")
		letter_array[0] = letter_array[0].upcase
		letter_array.join("")
	end
end
0
