class Timer
	attr_accessor :seconds
	def initialize
		@seconds = 0
	end

	def time_string
		hours = @seconds / (60 * 60)
		minutes = (@seconds % (60 * 60) / 60)
		secs = (@seconds % 60)
		[Timer.new_num(hours), Timer.new_num(minutes), Timer.new_num(secs)].join(":")
	end
	private

	def Timer.new_num(num)
		if (num < 10)
			"0#{num}"
		else
			num.to_s
		end
	end
end
